# LIS4331 - Advanced Mobile Web Application Development

## Michael DeMaria

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)


| README.md file should include the following items:          | git commands w/short descriptions                                           |
| ----------------------------------------------------------- | ----------------------------------------------------------------------------|
| Screenshots of running java Hello                           | 1. git init- create a new local repository                                  |
| Screenshot of running Android Studio - My first App         | 2. git status- list files you have changed                                  |
| Screenshots of running Android Studio - Contacts App        | 3. git add- add one or more files to index                                  |
| git commands w/ short descriptions                          | 4. git commit- commit changes                                               |
|                                                             | 5. git push- send changes to the master branch of your remote repos         |
|                                                             | 6. git pull- fetch and merge changes on remote server to working directory  |
|                                                             | 7. git config- tell Git who you are                                         |


#### Assignment Screenshots:

*Screenshot of running java Hello:*

![Java Hello](img/jdk_install.png)

*Screenshot of running Android Studio - My first App*

![Android Studio - My First App](img/android.png)

*Screenshot of Contacts App - Main Screen*
![Contacts App Main Screen](img/contacts_main.png)

*Screenshot of Contacts App - Info Screen*
![Contacts App Info Screen](img/contacts_info.png)

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mtd16/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
