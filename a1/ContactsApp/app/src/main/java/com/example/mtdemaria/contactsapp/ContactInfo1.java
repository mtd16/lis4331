package com.example.mtdemaria.contactsapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ContactInfo1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.my_ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        setContentView(R.layout.contact_1);
    }
}
