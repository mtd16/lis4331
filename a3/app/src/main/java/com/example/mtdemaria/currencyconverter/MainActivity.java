package com.example.mtdemaria.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton, euros, pesos, canadian;
    private Button convert;
    private EditText dollars;
    private Double amount = 1.00;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher_initials_round);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        addListenerOnButton();
    }

    private void addListenerOnButton() {

        radioGroup = findViewById(R.id.radioGroup);
        dollars = findViewById(R.id.enterAmount);
        euros = findViewById(R.id.euroButton);
        pesos = findViewById(R.id.pesoButton);
        canadian = findViewById(R.id.canadianButton);
        convert = findViewById(R.id.convertButton);
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView result = ((TextView) findViewById(R.id.result));

                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedId);

                if (dollars.getText().toString().trim().length() <= 0) {
                    Toast.makeText(MainActivity.this, "Please enter a dollar amount", Toast.LENGTH_SHORT).show();
                }

                else {
                    amount = Double.parseDouble(dollars.getText().toString());

                    if (dollars.getText().toString().trim().length() <= 0) {
                        Toast.makeText(MainActivity.this, "Please enter a dollar amount", Toast.LENGTH_SHORT).show();
                    }

                    else {
                        if (radioButton == euros) {
                            if ((amount > 0) && (amount < 100000)) {
                                amount *= 0.88;
                                result.setText(String.format("€%.2f Euros", amount));
                            }
                            else {
                                Toast.makeText(getApplicationContext(),  "US Dollars must be <=100,000 and > 0", Toast.LENGTH_SHORT).show();
                            }
                        } else if (radioButton == pesos) {
                            if ((amount > 0) && (amount < 100000)) {
                                amount *= 19.19;
                                result.setText(String.format("$%.2f Pesos", amount));
                            }
                            else {
                                Toast.makeText(getApplicationContext(),  "US Dollars must be <=100,000 and > 0", Toast.LENGTH_SHORT).show();
                            }

                        } else if (radioButton == canadian) {
                            if ((amount) > 0 && (amount < 100000)) {
                                amount *= 3.76;
                                result.setText(String.format("$%.2f Canadian", amount));
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "US Dollars must be <=100,000 and > 0", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                }



            }
        });
    }
}
