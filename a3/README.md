# LIS4331 - Advanced Mobile Web Application Development

## Michael DeMaria

### Assignment 3 Requirements:

*Two Parts:*

1. Backward engineer a Currency Converter app converts US Dollars to Pesos, Euros, and Canadian dollars

2. Chapter Questions (Chs 5, 6)


#### README.md file should include the following items:

* Screenshot of unpopulated Currency Converter app
* Screenshot of populated Currency Converter app
* Screenshot of Toast message

#### Assignment Screenshots:


*Screenshot of Unpopulated Tip Calculator App:*

![Unpopulated Tip Calculator App](img/unpopulated_app.png)

*Screenshot of Toast Message Error:*

![Toast Message Error](img/toast_message.png)

*Screenshot of Populated Tip Calculator App:*

![Populated Tip Calculator App](img/populated_app.png)
