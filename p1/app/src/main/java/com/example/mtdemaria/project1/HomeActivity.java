package com.example.mtdemaria.project1;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {
    Button button, button2, button3;
    MediaPlayer mpChance, mpMac, mpFuture;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //shows app icon
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);




        button2 = (Button) findViewById(R.id.macbutton2);
        button3 = (Button) findViewById(R.id.futurebutton3);
        button = (Button) findViewById(R.id.chancebutton);

        mpChance = MediaPlayer.create(HomeActivity.this, R.raw.chancee);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (playing) {
                    case 0:
                        mpChance.start();
                        playing = 1;
                        button.setText("Pause Chance");
                        button2.setVisibility(View.INVISIBLE);
                        button3.setVisibility(View.INVISIBLE);
                        break;

                    case 1:
                        mpChance.pause();
                        playing = 0;
                        button.setText("Play Chance");
                        button2.setVisibility(View.VISIBLE);
                        button3.setVisibility(View.VISIBLE);
                        break;

                }

            }
        });

        mpMac = MediaPlayer.create(HomeActivity.this, R.raw.macc);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (playing) {
                    case 0:
                        mpMac.start();
                        playing = 1;
                        button2.setText("Pause Mac Miller");
                        button.setVisibility(View.INVISIBLE);
                        button3.setVisibility(View.INVISIBLE);
                        break;

                    case 1:
                        mpMac.pause();
                        playing = 0;
                        button2.setText("Play Mac Miller");
                        button.setVisibility(View.VISIBLE);
                        button3.setVisibility(View.VISIBLE);
                        break;

                }

            }
        });

        mpFuture = MediaPlayer.create(HomeActivity.this, R.raw.liee);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (playing) {
                    case 0:
                        mpFuture.start();
                        playing = 1;
                        button3.setText("Pause Future");
                        button.setVisibility(View.INVISIBLE);
                        button2.setVisibility(View.INVISIBLE);
                        break;

                    case 1:
                        mpFuture.pause();
                        playing = 0;
                        button3.setText("Play Future");
                        button.setVisibility(View.VISIBLE);
                        button2.setVisibility(View.VISIBLE);
                        break;

                }

            }
        });





    }
}

