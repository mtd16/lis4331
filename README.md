# LIS4331 - Advanced Mobile Web Application Development

## Mike DeMaria

### LIS4331 Requirements:

*Coursework Links:*

# Hello

1. [A1 README.md](https://bitbucket.org/mtd16/lis4331/src/master/a1/README.md)
	* Install JDK
	* Install Android Studio and create My First App and Contacts App
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
	* Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/mtd16/lis4331/src/master/a2/)
	* Backward engineer a Tip Calculator app that splits the bill
	  between a group of people, including variable tip percentage
	* Provide screenshots of completed app
3. [A3 README.md](https://bitbucket.org/mtd16/lis4331/src/master/a3/)
	* Backward engineer a Currency Conversion app that converts US Dollars to Pesos, Euros, and Canadian dollars
	* Complete chapter 5, 6 Questions
4. [A4 README.md](https://bitbucket.org/mtd16/lis4331/src/master/a4/)
	* Backward engineer a Mortgage Interest Calculator Application
	* Complete chapter 11, 12 Questions
5. [A5 README.md](https://bitbucket.org/mtd16/lis4331/src/master/a5/)
	* Create an RSS Feed News Reader Android application
	* Complete textbook chapter questions
6. [P2 README.md](https://bitbucket.org/mtd16/lis4331/src/master/p2/)
	* Complete To Do Android Application using SQLite
