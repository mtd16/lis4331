package com.example.mtdemaria.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    double tipAmount = 0.0;
    double billAmount = 0.0;
    double totalCost = 0.0;
    int numGuests = 0;
    int tipChoice = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText bill = (EditText)findViewById(R.id.enterBillTotal);
        final Spinner guests = (Spinner) findViewById(R.id.spinner1);
        final Spinner tip = (Spinner)findViewById(R.id.spinner2);
        Button calculate = (Button)findViewById(R.id.calculate);

        calculate.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView) findViewById(R.id.txtResult));
            @Override
            public void onClick(View v) {
                guests.setPrompt("Number of guests");
                numGuests = Integer.parseInt(guests.getSelectedItem().toString());
                tipChoice = Integer.parseInt(tip.getSelectedItem().toString().replace("%", ""));

                if (tipChoice == 0) {
                    tipAmount = 1.00;
                } else if (tipChoice == 5) {
                    tipAmount = 1.05;
                } else if (tipChoice == 10) {
                    tipAmount = 1.1;
                } else if (tipChoice == 15) {
                    tipAmount = 1.15;
                } else if (tipChoice == 20) {
                    tipAmount = 1.2;
                } else if (tipChoice == 25) {
                    tipAmount = 1.25;
                }

                billAmount = Double.parseDouble(bill.getText().toString());
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
                totalCost = (billAmount * tipAmount) / numGuests;
                result.setText("Cost for " + numGuests + " guests: " + nf.format(totalCost));
            }
        });
    }
}
