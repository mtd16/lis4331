# LIS4331 - Advanced Mobile Web Application Development

## Michael DeMaria

### Assignment 2 Requirements:

*Two Parts:*

1. Backward engineer a Tip Calculator app that splits the bill between a group of people, including variable tip percentage, with the following requirements:
    * Drop-down menu for total number of guests (including yourself): 1 –10
    * Drop-down menu for tip percentage (5% increments): 0 – 25
    * Must add background color(s) or theme (10 pts)
    * Must create and display launcher icon image (10 pts)

2. Chapter Questions (Chs 3, 4)


#### README.md file should include the following items:

* Screenshots of unpopulated Tip Calculator app
* Screenshots of populated Tip Calculator app

#### Assignment Screenshots:


*Screenshot of Unpopulated Tip Calculator App:*

![Unpopulated Tip Calculator App](img/unpopulated_app.png)


*Screenshot of Populated Tip Calculator App:*

![Populated Tip Calculator App](img/populated_app.png)
