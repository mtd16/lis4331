# LIS4331 - Advanced Mobile Web Application Development

## Michael DeMaria

### Assignment 5 Requirements:

*Two Parts:*

1. Create a RSS Feed News Reader including 
    * Screenshotofrunning application’s splash screen(list of articles –activity_items.xml)
    * Screenshotofrunning application’s individual article(activity_item.xml)
    * Screenshotsofrunning application’sdefault browser(article link);

2. Chapter Questions


#### README.md file should include the following items:

* Screenshot of Splash Screen with list of news items
* Screenshot of News Item
* Screenshot of News Item in Browser

#### Assignment Screenshots:


| *Splash Screen with list of news items:*         | *News Item:*         | *News Page in Browser:*       |
|----------------------------------------|----------------------------------------|------------------------------------|
| ![Splash Screen with list of news items](images/rss_feed.png)| ![News Item](images/rss_item.png)|![News Page in Browser](images/rss_news_page.png) |
