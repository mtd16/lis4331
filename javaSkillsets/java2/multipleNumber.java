import java.util.Scanner;

public class multipleNumber
{
    public static void main (String args[])
    {
        System.out.printf("Program determines if first number is multiple of second, prints result.%n");
        System.out.printf("Example: 2, 4, 6, 8 are multiples of 2%n");
        System.out.printf("1) Use integers. 2) Use printf() function to print.%n");
        System.out.printf("Must *only* permit integer entry.%n");

        Scanner input = new Scanner(System.in);
        boolean isValid = true;
        int num1 = 0;
        int num2 = 0;

        do {
            System.out.printf("Num1: ");

            if (input.hasNextInt()) {
                num1 = input.nextInt();
            }

            else {
                isValid = false;
                System.out.printf("Not a valid integer!%n%n");
                System.out.printf("Please try again. ");
                input.next();
            }
        } while (!(isValid));
    }
}
