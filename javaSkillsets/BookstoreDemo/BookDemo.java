import java.util.Scanner;

class BookDemo
{
    public static void main(String[] args)
    {
        String code = "";
        String descr = "";
        String a = "";
        double p = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("\n////Below are default constructor values:////");
        Product p1 = new Product();

        System.out.println("Code = " + p1.getProductCode());
        System.out.println("Description = " + p1.getDescription());
        System.out.println("Price = " + p1.getPrice());

        System.out.println("\n////Below are user-entered values:////");

        System.out.print("\nCode: ");
        code = sc.nextLine();

        System.out.print("Description: ");
        descr = sc.nextLine();

        System.out.print("Price: ");
        p = sc.nextDouble();

        Product p2 = new Product(code, descr, p);
        System.out.println("\nCode = " + p2.getProductCode());
        System.out.println("Description = " + p2.getDescription());
        System.out.println("Price = $" + p2.getPrice());

        System.out.println("\n////Below using setter methods to pass literal values, then print() method to display values////\n");
        p2.setProductCode("xyz789");
        p2.setDescription("Test widget");
        p2.setPrice(89.99);
        p2.print();

        System.out.println("\n////Below are *derived class default constructor* values (instantiating b1; then using getter methods):////");

        Book b1 = new Book();
        System.out.println("Code = " + b1.getProductCode());
        System.out.println("Description = " + b1.getDescription());
        System.out.println("Price = " + b1.getPrice());
        System.out.println("Author = " + b1.getAuthor());

        System.out.println("\nOr using overridden derived class print() method...");
        b1.print();

        System.out.println("\n////Below are *derived class* using user-entered values (instantiating b2; then using getter methods):////");
        System.out.print("\nCode: ");
        code = sc.nextLine();
        code = sc.nextLine();

        System.out.print("Description: ");
        descr = sc.nextLine();

        System.out.print("Price: ");
        p = sc.nextDouble();

        System.out.print("Author: ");
        a = sc.nextLine();
        a = sc.nextLine();

        Book b2 = new Book(code, descr, p, a);
        System.out.println("Code = " + b2.getProductCode());
        System.out.println("Description = " + b2.getDescription());
        System.out.println("Price = $" + b2.getPrice());
        System.out.println("Author = " + b2.getAuthor());

        System.out.println("\nOr using derived class print() method...\n");
        b2.print();






    }
}
