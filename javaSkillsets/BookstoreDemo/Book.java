class Book extends Product
{
    private String author;

    public Book()
    {
        super();
        System.out.println("\nInside book default constructor.");
        author = "John Doe";
    }

    public Book(String pc, String desc, double prc, String a) //product cost, descr, price, author
    {
        super(pc, desc, prc);
        System.out.println("\nInside book constructor with parameters");
        author = a;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String a)
    {
        author = a;
    }

    public void print()
    {
        super.print();
        System.out.println("Author: " + author);
    }
}
