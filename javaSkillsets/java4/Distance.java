import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Distance implements ActionListener {
// BmiGui3 is its own runnable client program
public static void main(String[] args) {
 Distance gui = new Distance();
 }

// onscreen components stored as fields
private JFrame frame;
private JTextField heightField;
private JTextField weightField;
private JLabel bmiLabel;
private JButton computeButton;

public Distance() {
 // set up components
heightField = new JTextField(5);
weightField = new JTextField(5);
bmiLabel = new JLabel("Compute Distance Leg C.");
computeButton = new JButton("Compute");

// attach GUI as event listener to Compute button
computeButton.addActionListener(this);

// layout
JPanel north = new JPanel(new GridLayout(2, 2));
north.add(new JLabel("Leg A: "));
north.add(heightField);
north.add(new JLabel("Leg B: "));
north.add(weightField);

// overall frame
frame = new JFrame("Leg C: ");
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setLayout(new BorderLayout());
frame.add(north, BorderLayout.NORTH);
frame.add(bmiLabel, BorderLayout.CENTER);
frame.add(computeButton, BorderLayout.SOUTH);
frame.pack();
frame.setVisible(true);
}

 // Handles clicks on Compute button by computing the BMI.
public void actionPerformed(ActionEvent event) {
// read height/weight info from text fields
String heightText = heightField.getText();
double height = Double.parseDouble(heightText);
String weightText = weightField.getText();
double weight = Double.parseDouble(weightText);

// compute BMI and display it onscreen
double bmi = Math.sqrt(height * height + weight * weight);
bmiLabel.setText("Leg C: " + bmi);
}
}
