import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class MultipleSelectionFrame extends JFrame
{
    private JList<String> colorJList;
    private JList copyJList;
    private JButton copyJButton;
    private static final String[] colors = { "red", "blue", "green", "orange", "purple",
                                             "black", "white", "brown", "gray", "pink" };

    public MultipleSelectionFrame()
    {
        super("Multiple Selection Lists");
        setLayout(new FlowLayout());
        colorJList = new JList<>(colors);
        colorJList.setVisibleRowCount(5);
        colorJList.setFixedCellWidth(140);
        colorJList.setFixedCellHeight(15);
        colorJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        add(new JScrollPane(colorJList));
        copyJButton = new JButton("Copy >>>");
        copyJButton.addActionListener(
            new ActionListener()
            {
                public void actionPerformed(ActionEvent event)
                {
                    copyJList.setListData(colorJList.getSelectedValues());
                }
            }
        );

        add(copyJButton);
        copyJList = new JList();

        copyJList.setVisibleRowCount(5);
        copyJList.setFixedCellWidth(140);
        copyJList.setFixedCellHeight(15);
        copyJList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        add(new JScrollPane(copyJList));
    }
}
