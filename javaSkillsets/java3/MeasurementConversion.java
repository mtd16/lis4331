import java.util.Scanner;

public class MeasurementConversion
{


    public static void main(String args[])
    {
        int numberOfInches = 0;
        boolean isNumber = false;
        Scanner input = new Scanner(System.in);


        System.out.printf("Program converts inches to centimeters, meters, feet, yards, and miles.\n");
        System.out.printf("***Notes***:\n");
        System.out.printf("1) Use integer for inches (must validate integer input).\n");
        System.out.printf("2) Use printf() function to print (format values per below output).\n");
        System.out.printf("3) Create Java 'constants' for the following values:\n");
        System.out.printf("\tINCHES_TO_CENTIMETER,\n");
        System.out.printf("\tINCHES_TO_METER,\n");
        System.out.printf("\tINCHES_TO_FOOT,\n");
        System.out.printf("\tINCHES_TO_YARD,\n");
        System.out.printf("\tFEET_TO_MILE\n\n");

        do {
            System.out.printf("Please enter number of inches: ");

            if (input.hasNextInt()) {
                numberOfInches = input.nextInt();
                isNumber = true;
            }

            else {
                System.out.printf("Not a valid number!\n\n");
                input.next();
            }
        } while (!(isNumber));

        double INCHES_TO_CENTIMETER = numberOfInches * 2.54;
        double INCHES_TO_METER = numberOfInches * 0.0254;
        double INCHES_TO_FOOT = numberOfInches * 0.0833333;
        double INCHES_TO_YARD = numberOfInches * 0.0277778;
        double FEET_TO_MILE = INCHES_TO_FOOT * 0.000189394;

        System.out.printf("%d inch(es) equals%n%n", numberOfInches);

        System.out.printf("%.5f centimeter(s)%n", INCHES_TO_CENTIMETER);
        System.out.printf("%.5f meter(s)%n", INCHES_TO_METER);
        System.out.printf("%.5f feet%n", INCHES_TO_FOOT);
        System.out.printf("%.5f yard(s)%n", INCHES_TO_YARD);
        System.out.printf("%.8f mile(s)%n%n", FEET_TO_MILE);










    }
}
