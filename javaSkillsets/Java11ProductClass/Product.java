class Product
{
    private String productCode;
    private String description;
    private double price;


    public Product()
    {
        System.out.println("\nInside product default constructor.");
        productCode = "abc123";
        description = "My widget";
        price = 49.99;
    }

    public Product(String productCode, String description, double price)
    {
        System.out.println("\nInside product constructor with parameters.");
        this.productCode = productCode;
        this.description = description;
        this.price = price;
    }

    public String getProductCode()
    {
        return productCode;
    }

    public void setProductCode(String pc)
    {
        productCode = pc;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String desc)
    {
        description = desc;
    }

    public double getPrice()
    {
        return price;
    }
    public void setPrice(double prc)
    {
        price = prc;
    }

    public void print()
    {
        System.out.println("Code: " + productCode + ", Description: " + description + ", Price: $" + price);
    }
}
