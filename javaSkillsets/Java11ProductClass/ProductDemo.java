import java.util.Scanner;

class ProductDemo
{
    public static void main(String[] args)
    {
        String code = "";
        String descr = "";
        double p = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("\n////Below are default constructor values:////");
        Product p1 = new Product();

        System.out.println("Code = " + p1.getProductCode());
        System.out.println("Description = " + p1.getDescription());
        System.out.println("Price = " + p1.getPrice());

        System.out.println("\n////Below are user-entered values:////");

        System.out.print("\nCode: ");
        code = sc.nextLine();

        System.out.print("Description: ");
        descr = sc.nextLine();

        System.out.print("Price: ");
        p = sc.nextDouble();

        Product p2 = new Product(code, descr, p);
        System.out.println("\nCode = " + p2.getProductCode());
        System.out.println("Description = " + p2.getDescription());
        System.out.println("Price = $" + p2.getPrice());

        System.out.println("\n////Below using setter methods to pass literal values, then print() method to display values////\n");
        p2.setProductCode("xyz789");
        p2.setDescription("Test widget");
        p2.setPrice(89.99);
        p2.print();
    }
}
