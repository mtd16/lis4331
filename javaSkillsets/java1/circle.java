import java.util.Scanner;

public class circle
{
   public static void main(String args[])
   {
      System.out.println("Non-OOP program calculates diameter, circumference, and circle area.");
      System.out.println("Must use Java's built-in PI constant, printf(), and formatted to 2 decimal places.");
      System.out.println("Must *only* permit numeric entry.");

      Scanner input = new Scanner(System.in);
      int number = 0;
      boolean isNumber = true;



      do {

          System.out.println("");
          System.out.print("Enter circle radius: ");

          if (input.hasNextInt()) {
                number = input.nextInt();
                isNumber = true;
          }

          else {
              System.out.println("Not valid number!");
              System.out.println("Please try again.");
              isNumber = false;
              input.next();
          }

      } while (!(isNumber));

      //Calculations
      double diamter = (2 * number);
      double area = Math.PI * (number * number);
      double circumference= Math.PI * 2 * number;
      //Results
      System.out.println();
      System.out.printf("Circle diamter: %.2f" , diamter);
      System.out.println();
      System.out.printf("Circumference: %.2f" , circumference);
      System.out.println();
      System.out.printf("Area: %.2f" , area);
      System.out.println();
   }
}
