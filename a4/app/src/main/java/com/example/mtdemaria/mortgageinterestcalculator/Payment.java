package com.example.mtdemaria.mortgageinterestcalculator;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Payment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher_initials);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);
        TextView monthlyPayment = (TextView)findViewById(R.id.MonthlyPayment);
        ImageView image = (ImageView) findViewById(R.id.imgYears);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        float decPayment = sharedPref.getFloat("key1", 0);
        int intYears = sharedPref.getInt("key2", 0);
        float decPrincipal = sharedPref.getFloat("key3", 0);
        float decTotalPayment;

        if (intYears == 10) {
            decTotalPayment = (decPayment * 120) - decPrincipal;
            DecimalFormat currency = new DecimalFormat("$###,###.##");
            monthlyPayment.setText("Total paid: " + currency.format(decTotalPayment));
            image.setImageResource(R.drawable.ten);
        }
        else if (intYears == 20) {
            decTotalPayment = (decPayment * 240) - decPrincipal;
            DecimalFormat currency = new DecimalFormat("$###,###.##");
            monthlyPayment.setText("Total paid: " + currency.format(decTotalPayment));
            image.setImageResource(R.drawable.twenty);
        }

        else if (intYears == 30) {
            decTotalPayment = (decPayment * 360) - decPrincipal;
            DecimalFormat currency = new DecimalFormat("$###,###.##");
            monthlyPayment.setText("Total paid: " + currency.format(decTotalPayment));
            image.setImageResource(R.drawable.thirty);
        }
        else {
            monthlyPayment.setText("Enter 10, 20, or 30 years");
            image.setImageResource(R.drawable.mortgage);
        }
    }
}
