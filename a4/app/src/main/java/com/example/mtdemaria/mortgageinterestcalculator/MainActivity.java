package com.example.mtdemaria.mortgageinterestcalculator;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.content.SharedPreferences;

public class MainActivity extends AppCompatActivity {

    float decPayment;
    int intYears;
    float decPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher_initials);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText payment = (EditText) findViewById(R.id.txtPayment);
        final EditText years = (EditText) findViewById(R.id.txtYears);
        final EditText principal = (EditText) findViewById(R.id.txtPrincipal);

        Button calculate = (Button) findViewById(R.id.btnCalculate);

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decPayment = Float.parseFloat(payment.getText().toString());
                intYears = Integer.parseInt(years.getText().toString());
                decPrincipal = Float.parseFloat(principal.getText().toString());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putFloat("key1", decPayment);
                editor.putInt("key2", intYears);
                editor.putFloat("key3", decPrincipal);
                editor.commit();
                startActivity(new Intent(MainActivity.this, Payment.class));
            }
        });
    }
}
