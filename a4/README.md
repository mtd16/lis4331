# LIS4331 - Advanced Mobile Web Application Development

## Michael DeMaria

### Assignment 4 Requirements:

*Two Parts:*

1. Backward engineer a Mortgage Interest Calculator Android Application
2. Include splash screen image, app title, intro text.
3. Include appropriate images.
4. Must use persistent data: SharedPreferences
5. Widgets and images must be vertically and horizontally aligned.
6. Must add background color(s) or theme
7. Create and display launcher icon image

2. Chapter Questions (Chs 11, 12)


#### README.md file should include the following items:

* Screenshot of Splash Screen
* Screenshot of Invalid Entry
* Screenshot of Valid Entry

#### Assignment Screenshots:


| *Screenshot of Splash Screen:*         | *Screenshot of Invalid Entry:*         | *Screenshot of Valid Entry:*       |
|----------------------------------------|----------------------------------------|------------------------------------|
| ![Splash Screen](img/splash_screen.png)| ![Invalid Entry](img/invalid_entry.png)|![Valid Entry](img/valid_entry.png) |
