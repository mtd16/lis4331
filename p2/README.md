# LIS4331 - Advanced Mobile Web Application Development

## Michael DeMaria

### Project 2 Requirements:

*Two Parts:*

1. Insert at least five sample tasks
2. Test database
3. Must add background colors or theme
4. Create Display launcher icon image

###README.md should include the following items:
1. Course title, your name, assignment requirements, as per A1
2. Screenshot of running application's Task List

### Deliverables


*Screenshot of Task List:*

| *Splash Screen with list of to do app:*         | *Running app:*         |
|----------------------------------------|----------------------------------------|
| ![Splash Screen](images/splashscreen.png)| ![](images/tasklist2.png)| |
